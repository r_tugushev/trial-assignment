﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApplication5;
using WindowsFormsApplication5.MyServiceReference;

namespace Server.Gui
{
    public class MyServiceCallback : IMyServiceCallback
    {
        public void OnCallback(string xml)
        {
            Utility.LoadDataToTreeView(xml, Utility.treeView);
        }
    }
}
