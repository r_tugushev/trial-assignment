﻿using Server.Gui;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using WindowsFormsApplication5.MyServiceReference;

namespace WindowsFormsApplication5
{
    public static class Utility
    {
        public static TreeView treeView;

        //public static MyServiceClient client;
        public static void StartService()
        {
            var callback = new MyServiceCallback();
            var instanceContext = new InstanceContext(callback);
            MyServiceClient client = new MyServiceClient(instanceContext);
            client.OpenSession();
        }

        public static void LoadDataToTreeView(string xmlText, TreeView tv)
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(xmlText);

            tv.Nodes.Clear();

            ConvertXmlNodeToTreeNode(xml, tv.Nodes);
            tv.Nodes[0].ExpandAll();
        }

        private static void ConvertXmlNodeToTreeNode(XmlNode xmlNode, TreeNodeCollection treeNodes)
        {
            TreeNode newTreeNode = treeNodes.Add(xmlNode.Name, xmlNode.Value);

            switch (xmlNode.NodeType)
            {
                case XmlNodeType.Element:

                    newTreeNode.Text = xmlNode.Name;
                    break;
                case XmlNodeType.Text:
                case XmlNodeType.CDATA:
                    newTreeNode.Text = xmlNode.Value;
                    break;
            }

            if (xmlNode.Attributes != null)
            {
                foreach (XmlAttribute attribute in xmlNode.Attributes)
                {
                    ConvertXmlNodeToTreeNode(attribute, newTreeNode.Nodes);
                }
            }
            foreach (XmlNode childNode in xmlNode.ChildNodes)
            {
                ConvertXmlNodeToTreeNode(childNode, newTreeNode.Nodes);
            }
        }
    }
}