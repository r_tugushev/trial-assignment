﻿using Server.Service;
using System;
using System.Windows.Forms;
using System.Xml;

namespace Server.Gui
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoadDataToTreeView();

        }

        private void LoadDataToTreeView()
        {
            XmlDocument xml;

            using (XmlReader reader = XmlReader.Create(textBox1.Text))
            {
                xml = new XmlDocument();
                xml.Load(reader);
            }


            treeView1.Nodes.Clear();

            ConvertXmlNodeToTreeNode(xml, treeView1.Nodes);
            treeView1.Nodes[0].ExpandAll();

            treeView1.ContextMenuStrip = contextMenuStrip1;

            Service.BroadcastMessage(treeView1);

        }

        private void ConvertXmlNodeToTreeNode(XmlNode xmlNode,
  TreeNodeCollection treeNodes)
        {
            TreeNode newTreeNode = treeNodes.Add(xmlNode.Name, xmlNode.Value);

            switch (xmlNode.NodeType)
            {
                case XmlNodeType.Element:

                    newTreeNode.Text = xmlNode.Name;
                    break;
                case XmlNodeType.Text:
                case XmlNodeType.CDATA:
                    newTreeNode.Text = xmlNode.Value;
                    break;
            }

            if (xmlNode.Attributes != null)
            {
                foreach (XmlAttribute attribute in xmlNode.Attributes)
                {
                    ConvertXmlNodeToTreeNode(attribute, newTreeNode.Nodes);
                }
            }
            foreach (XmlNode childNode in xmlNode.ChildNodes)
            {
                ConvertXmlNodeToTreeNode(childNode, newTreeNode.Nodes);
            }
        }

        private void удалитьToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            if(treeView1.SelectedNode.Nodes.Count > 0)
            {
                treeView1.SelectedNode.Remove();
            }
            else
            {
                treeView1.SelectedNode.Name = null;
                treeView1.SelectedNode.Text = null;
            }

            Service.BroadcastMessage(treeView1);
        }

        private void переименоватьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string input = Promt.ShowDialog("Переименование", "Введите новое значение");
            treeView1.SelectedNode.Name = input;
            treeView1.SelectedNode.Text = input;

            Service.BroadcastMessage(treeView1);
        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string input = Promt.ShowDialog("Новый узел", "Введите название нового узла");
            treeView1.SelectedNode.Nodes.Add(input, input);

            Service.BroadcastMessage(treeView1);
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string input = Promt.ShowDialog("Новый узел", "Введите название нового узла");
            TreeNode tv = treeView1.SelectedNode.Nodes.Add(input, input);
            tv.Nodes.Add("", "");

            Service.BroadcastMessage(treeView1);
        }

    }
}
