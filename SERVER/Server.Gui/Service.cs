﻿using Server.Service;
using System.Windows.Forms;

namespace Server.Gui
{
    public class Service
    {
        public static void BroadcastMessage(TreeView tv)
        {
            if (MyService.Callback != null)
            {
                string result = Utility.ConvertTreeViewToString(tv);
                Server.Service.MyService.Callback.OnCallback(result);
            }
        }
    }
}
