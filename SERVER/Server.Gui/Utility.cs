﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Server.Gui
{
    internal static class Utility
    {
        public static string ConvertTreeViewToString(TreeView tv)
        {
            StringBuilder sb = new StringBuilder();
            foreach (TreeNode node in tv.Nodes)
            {
                saveNode(node.Nodes, sb);
            }

            return sb.ToString();
        }

        private static void saveNode(TreeNodeCollection tnc, StringBuilder sb)
        {
            foreach (TreeNode node in tnc)
            {

                if (node.Nodes.Count > 0)
                {
                    sb.Append("\n <" + node.Text + ">");
                    saveNode(node.Nodes, sb);
                    sb.AppendLine("\n </" + node.Text + ">");

                }
                else
                    sb.Append(node.Text);
            }
        }
    }
}
