﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Server.Service
{

    [ServiceContract(CallbackContract = typeof(IMyServiceCallback), SessionMode = SessionMode.Required)]
    public interface IMyService
    {
        [OperationContract]
        void OpenSession();
    }

    public interface IMyServiceCallback
    {
        [OperationContract(IsOneWay = true)]
        void OnCallback(String xml);
    }
}
