﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Server.Service
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant, InstanceContextMode = InstanceContextMode.Single)]
    public class MyService : IMyService
    {
        public static IMyServiceCallback Callback;

        public void OpenSession()
        {
            IMyServiceCallback cb = OperationContext.Current.GetCallbackChannel<IMyServiceCallback>();
            Callback = OperationContext.Current.GetCallbackChannel<IMyServiceCallback>();
        }
    }
}
